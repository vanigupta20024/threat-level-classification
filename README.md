# Threat-Level-Classification

**Dataset:** logevent.csv <br>
**Model:** To predict the incoming security threat is advanced or not

#### Requirements:
##### Packages:
```
category_encoders
pandas
scikit-learn
```
##### Softwares/Tools:
```
Anaconda prompt
Jupyter Notebook
```

#### Symmary:

Model first trains on the dataset with labelled data. Model used is **Desicion Tree Classifier**. This will highly reduce the SOCs threat attending time since it will tell which are the advanced threats so that these could be catered first. <br>
The dataset is self-generated using random logic just to show a sample working of how this could be implemented in the future and by no means should be assumed to be a real dataset.

#### Model Accuracy:
```
78.8%
```